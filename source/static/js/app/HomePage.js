import $ from 'jquery';
import select2 from 'select2';

class HomePage {
	constructor() {
		this._css = {
		};
        
        this.$form = $('body').find('form');
        this.$inputs = this.$form.find('input');
        this.submitBtn = $('.js-submit-btn');
        this.closePopup = $('.js-btn-continue');
        this.select2Field = $('.select2-container--default .select2-selection--single');

        this._initSelect2();

        this.$inputs.on('focusin focusout', (e) => {
            this._handleFocus(e);
        });

        this.submitBtn.on('click', (e) => {
            this._handleBtnClick(e);
        });

        this.closePopup.on('click', (e) => {
            this._handleClosePopuClick(e);
        });
	};

    _initSelect2(){
        let select = $('select');
        select.select2();

        select.on("select2:open", function(e) {
            $('.select2-selection').removeClass('error');   
            $('select[name="country"]').next().next().removeClass('show');
        })
    }

    _handleFocus(e) {
        let $target = $(e.currentTarget);

        switch(e.type) {
            case 'focusin': {
                $target.removeClass('error');
                $target.next().removeClass('show');
                break;
            }
            case 'focusout': {
                break;
            }
        }
    }

    _handleClosePopuClick() {
        let successMess = $('.pop-up');

        successMess.removeClass('show');        
    }

	_handleBtnClick(e){
		let submitBtn = $('.js-submit-btn');
        let nameInput = $('input[name="name"]');
        let phoneInput = $('input[name="phone"]');
        let mailInput = $('input[name="email"]');
        let select = $('select[name="country"]');
        let selectFild = $('select[name="country"] option:selected').text();
        let errorMess = $('.error-massage');
        let successMess = $('.pop-up');

        e.preventDefault();
        
        const testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
        if(nameInput.val() == ''){
            nameInput.addClass('error');
            nameInput.next().addClass('show');
        } else {
            nameInput.removeClass('error');
            nameInput.next().removeClass('show');
        }
        if(phoneInput.val() == ''){
            phoneInput.addClass('error');
            phoneInput.next().addClass('show');
        } else {
            phoneInput.removeClass('error'); 
            phoneInput.next().removeClass('show');
        }
        if(mailInput.val() == '' || testEmail.test(mailInput)){
            mailInput.addClass('error');
            mailInput.next().addClass('show');
        } else{
            mailInput.removeClass('error'); 
            mailInput.next().removeClass('show');
        }
        if(selectFild == ''){
            $('.select2-container--default .select2-selection--single').addClass('error');
            select.next().next().addClass('show');
        } else{
            $('.select2-container--default .select2-selection--single').removeClass('error'); 
            select.next().next().removeClass('show');
        }
        
        $.ajax({
            url: this.$form.attr('action'),
            type: this.$form.attr('method'),
            dataType: 'json',
            data: this.$form.serialize(),
        
            success: (response) => {
                console.log(1, response);
                // if(response.success == false && errors.form.length){
                if(response.success == false){
                   console.log(response);
                } else{
                    console.log('yes');
                }
            }
        })
        
    }

	/**
	 * @public
	 * 
	 */
	resize() {
	}
};

export default HomePage;