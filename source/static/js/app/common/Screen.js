import 'plugins/modernizr-custom.js';
import verge from 'plugins/verge.js';

export default class Screen {
	constructor() {
        
	};

	/**
     *
     * @returns {boolean}
     */
    static get isTouchable () {
        return Screen._IS_TOUCHABLE;
    };

    /**
     *
     * @returns { {w, h} }
     */
    static get viewportSize () {
        return {
            w: verge.viewportW(),
            h: verge.viewportH()
        }
    };

    /**
     *
     * @returns {String}
     */
    static get state () {
        var vpW = verge.viewportW();

        if ( vpW >= 1025 ) {
            return Screen.DESKTOP_STATE;
        } else if ( vpW < 768 ) {
            return Screen.MOBILE_SMALL_STATE;
        } else if ( vpW < 961 ) {
            return Screen.MOBILE_STATE;
        } else {
            return Screen.TABLET_STATE;
        }
    };

    static get isIE () {
        return (/*@cc_on!@*/false || !!document.documentMode) || (/Edge\/\d./i.test(navigator.userAgent));
    };

    static get isFF () {
        return (false || (navigator.userAgent.toLowerCase().indexOf('firefox') > -1))
    }
}

Screen._IS_TOUCHABLE = document.querySelector( '.touchevents' ) != null;
Screen.DESKTOP_STATE = 'desktop_state';
Screen.TABLET_STATE = 'tablet_state';
Screen.MOBILE_SMALL_STATE = 'mobile_small_state';
Screen.MOBILE_STATE = 'mobile_state';